<?php

function menu() {
    echo '<nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.php?accion=index">
                        <img  id="logo" alt="Brand" src="img/icono.png">
                    </a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="index.php?accion=index">INICIO <span class="sr-only">(current)</span></a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">EMPRESA <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="index.php?accion=quienes">Quienes Somos</a></li>
                                <li><a href="index.php?accion=donde">Donde Estamos</a></li>

                            </ul>
                        </li>
                    </ul>

                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav> ';
}

function contenido() {
    echo '<div class="jumbotron">';
    echo '<div class="container">';
    echo '<h1>Bienvenidos a mi Empresa</h1>';
    echo '<p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.'
    . 'Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>';
    '</div>';
    '</div>';
}

function fotos() {
    echo '<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <div class="item active">
      <img src="img/1.jpg" alt="obras">
    </div>

    <div class="item">
      <img src="img/2.jpg" alt="obras">
    </div>

    <div class="item">
      <img src="img/3.jpg" alt="obras">
    </div>
  </div>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">Next</span>
  </a>
</div>';
}

function cabecera() {
    echo '<div class="page-header">
  <h1>CONOCENOS <small>Estas son nuestras obras</small></h1>
</div>';
}

function miniaturas() {
    echo '<div class="row">
        
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="img/1.jpg" alt="">
      <div class="caption">
        <h3>Thumbnail label</h3>
        <p>Pero esto que es!!!!</p>
        <p><a href="#" class="btn btn-primary" role="button">Obra</a> <a href="#" class="btn btn-default" role="button">Trabajos</a></p>
      </div>
    </div>
  </div>
  
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="img/2.jpg" alt="">
      <div class="caption">
        <h3>Thumbnail label</h3>
        <p>Esto otro no se como nos va a quedar</p>
        <p><a href="#" class="btn btn-primary" role="button">Obra</a> <a href="#" class="btn btn-default" role="button">Trabajos</a></p>
      </div>
    </div>
  </div>
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="img/3.jpg" alt="">
      <div class="caption">
        <h3>Thumbnail label</h3>
        <p>Prevencion de riesgos a tope</p>
        <p><a href="#" class="btn btn-primary" role="button">Obra</a> <a href="#" class="btn btn-default" role="button">Trabajos</a></p>
      </div>
    </div>
  </div>
</div>';
}

function mapaSituacion(){
    
   echo '<div class="container-fluid">
    <div class="map-responsive">
   <iframe src="https://www.google.com/maps/embed/v1/place?key=AIzaSyA0s1a7phLN0iaD6-UE7m4qP-z21pH0eSc&q=ohl+cantabria+españa" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>
</div>';
}
