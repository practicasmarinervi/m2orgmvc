<?php
include "componentes/componentes.php"; //no es una funcion. 
////las variable que estan aqui estaran tambien en la vista llamada
?>

<!doctype html>

<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <style>
            body {
                padding-top: 50px;
                padding-bottom: 20px;
            }

        </style>
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/main.css">
        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>
    <body>
        <div class="menu">
       <?php
       menu();
        ?>
            </div>
       
       <?php
       function accionIndex(){
           include "views/inicio.php";
       }
       function accionQuienes(){
           include "views/quienesSomos.php";
       }
       function accionDonde(){
           include "views/dondeEstamos.php";
       }
       
       if($_REQUEST['accion']=='quienes'){
           accionQuienes();
           
       } elseif($_REQUEST['accion']=='donde'){
           accionDonde();
           
       }else{
           accionIndex();
       }
       
        ?>
            
        
     <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

        <script src="js/vendor/bootstrap.min.js"></script>

        <script src="js/main.js"></script>
   
        
        
    </body>
</html>
